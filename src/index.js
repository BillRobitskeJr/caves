/**
 * Caves Engine entry module
 * @copyright Bill Robitske, Jr. 2017
 * @author    Bill Robitske, Jr. <bill.robitske.jr@gmail.com>
 * @license   MIT
 */

const Game = require('./engine/game');

window.Caves = {
  game: new Game()
};
