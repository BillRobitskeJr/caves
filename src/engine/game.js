/**
 * Caves Engine - Main game controller class
 * @copyright Bill Robitske, Jr. 2017
 * @author    Bill Robitske, Jr. <bill.robitske.jr@gmail.com>
 * @license   MIT
 */

const Player = require('./player');

/**
 * Game controller class
 */
class Game {
  
  /**
   * Create a new game controller
   */
  constructor() {
    this._player = new Player(this);
  }

  /**
   * Get this game's player
   * @returns {Player}  Current player
   */
  get player() {
    return this._player;
  }
}

module.exports = Game;
