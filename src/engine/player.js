/**
 * Caves Engine - Player character class
 * @copyright Bill Robitske, Jr. 2017
 * @author    Bill Robitske, Jr. <bill.robitske.jr@gmail.com>
 * @license   MIT
 */

const Character = require('./character');

/**
 * Player character class
 */
class Player extends Character {

  /**
   * Create a new player character
   * @param {Game}  game - Main game controller
   */
  constructor(game) {
    super(game);
  }
}

module.exports = Player;
