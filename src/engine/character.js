/**
 * Caves Engine - Abstract character class
 * @copyright Bill Robitske, Jr. 2017
 * @author    Bill Robitske, Jr. <bill.robitske.jr@gmail.com>
 * @license   MIT
 */

 /**
  * Character class
  */
class Character {

  /**
   * Create a new Character instance
   */
  constructor(game) {
    this._game = game;
  }
}

module.exports = Character;
