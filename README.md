# Caves

A text-based adventure game engine

## Project Goals

- Recreate the feel of text-based adventure games from the 1980s
- Serve as a tutorial and time capsule for future hopeful game designers
- Expand to allow for decentralized, multiplayer worlds
- Expand to allow multiple "views" of a world, from text-based, to 2D and 3D,
  and potentially even VR
