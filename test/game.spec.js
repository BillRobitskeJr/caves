/**
 * Game unit testing
 */

const expect = require('chai').expect;

const Game = require('../src/engine/game');
const Player = require('../src/engine/player');

describe('Game', function() {

  describe('Initial setup', function() {

    context('when created', function() {
      before(function() {
        this.game = null;
        this.createGame = () => { this.game = new Game(); };
      });

      it('should throw no errors', function() {
        expect(this.createGame).to.not.throw();
        expect(this.game).to.be.an.instanceof(Game);
      });
      it('should provide a .player property that returns a Player', function() {
        expect(this.game).to.have.property('player');
        expect(this.game.player).to.be.an('object').that.is.an.instanceof(Player);
      });
    });
  });
});
